package com.example.iyoteam.ethioroommate.actors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 4/13/2018.
 */

@SuppressWarnings("serial")
public class Characteristic implements Serializable {

    @SerializedName("religion")
    @Expose
    private Religion religion;

    @SerializedName("detail")
    @Expose
    private String detail;

    @SerializedName("current_job")
    @Expose
    private String job;

    @SerializedName("current_income")
    @Expose
    private Double income;

    @SerializedName("languages")
    @Expose
    private List<Language> languages;

    @SerializedName("likes")
    @Expose
    private List<Interest> likes;

    @SerializedName("dislikes")
    @Expose
    private List<Interest> dislikes;

    public Characteristic() {
    }

    public Characteristic(String job, Double income, Religion religion,
                          ArrayList<Language> languages, ArrayList<Interest> likes, ArrayList<Interest> dislikes){
        this.job = job;
        this.income = income;
        this.religion = religion;
        this.languages = languages;
        this.likes = dislikes;
        this.dislikes = dislikes;

    }

    public String getReligion() {
        return religion.getName();
    }

    public void setReligion(Religion religion) {
        this.religion = religion;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<Interest> getLikes() {
        return likes;
    }

    public void setLikes(List<Interest> likes) {
        this.likes = likes;
    }

    public List<Interest> getDislikes() {
        return dislikes;
    }

    public void setDislikes(List<Interest> dislikes) {
        this.dislikes = dislikes;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
