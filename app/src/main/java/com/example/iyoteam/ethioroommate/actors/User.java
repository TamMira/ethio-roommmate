package com.example.iyoteam.ethioroommate.actors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mikee on 4/1/2018.
 */

@SuppressWarnings("serial")
public class User implements Serializable{

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("fname")
    @Expose
    private String fname;

    @SerializedName("lname")
    @Expose
    private String lname;

    @SerializedName("user_name")
    @Expose
    private String userName;

    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("age")
    @Expose
    private Integer age;

    @SerializedName("characteristic")
    @Expose
    private Characteristic characteristic;

    @SerializedName("sex")
    @Expose
    private Integer sex;

    public User(String fname, String lname, String userName, String phoneNumber, String password, Integer age, Integer sex) {
        this.fname = fname;
        this.lname = lname;
        this.userName = userName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.age = age;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getUserName() {
        return userName;
    }

    public String getFullName() {
        return fname + " " + lname;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Characteristic getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(Characteristic characteristic) {
        this.characteristic = characteristic;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getSexName() {
        String _sex = "";
        switch (this.sex) {
            case 0:
                _sex = "Female";
                break;
            case 1:
                _sex = "Male";
                break;
            default:
                break;
        }

        return _sex;
    }


}
