package com.example.iyoteam.ethioroommate.dbhandlers;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.example.iyoteam.ethioroommate.activities.AddCharacterActivity;
import com.example.iyoteam.ethioroommate.activities.SignUpActivity;
import com.example.iyoteam.ethioroommate.activities.UserListActivity;
import com.example.iyoteam.ethioroommate.actors.Characteristic;
import com.example.iyoteam.ethioroommate.actors.Interest;
import com.example.iyoteam.ethioroommate.actors.Language;
import com.example.iyoteam.ethioroommate.actors.Religion;
import com.example.iyoteam.ethioroommate.actors.User;
import com.example.iyoteam.ethioroommate.network_apis.UserAPI;
import com.example.iyoteam.ethioroommate.R;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by iyob on 3/25/2018.
 */

public class UserDBHandler {

    private final String BASE_URL = "http://192.168.43.42";
    Context context;

    private Retrofit retrofit;
    private UserAPI userAPI;

    HttpLoggingInterceptor logging;

    OkHttpClient.Builder httpClient;

    /**
     * Establish a connection to server using RoomAPI class     */
    public UserDBHandler(Context context) {
        this.context = context;

        logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient= new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        try {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();

            //Creating object for our interface
            userAPI = retrofit.create(UserAPI.class);
        } catch (Exception ex) {
            Toast.makeText(context, "Error connecting to server please try again!", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * register new user
     * if registeration is succesfull show user list
     * else show failed message
     * @param user
     */

    public void insertUser(User user) {

        Call<Void> request = userAPI.insertUser(
                user.getFname(),
                user.getLname(),
                user.getUserName(),
                user.getPhoneNumber(),
                user.getPassword(),
                user.getSex(),
                user.getAge()
        );

        request.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(context, "Succesfully registered", Toast.LENGTH_LONG).show();
                ((SignUpActivity) context).clearForm();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Snackbar.make(((Activity) context).findViewById(R.id.signUpWrapper), context.getResources().getString(R.string.something_wrong), Snackbar.LENGTH_LONG).show();
                throwable.printStackTrace();
            }

        });

    }


    public void addUserCharacterisitic(Integer userId, Characteristic characteristic){

        Call<User> request = userAPI.addUserCharacter(characteristic);
        request.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Toast.makeText(context, response.body().getFullName(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<User> call, Throwable throwable) {
                Toast.makeText(context, "On failure ",Toast.LENGTH_LONG).show();
            }
        });

    }
    /**
     *
     * Get available users from the server
     * populate the recyclerView with retrieved users
     */
    public void getUserList() {

        Call<List<User>> request = userAPI.getUsers();

        request.enqueue(new Callback<List<User>>() {

            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
               ((UserListActivity) context).showUsers(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable throwable) {
                Snackbar.make(((Activity) context).findViewById(R.id.uploadListWrapper), "Something went wrong", Snackbar.LENGTH_LONG).show();
            }

        });
    }

    /**
     *
     * Get available users from the server
     * populate the recyclerView with retrieved users
     */
    public void getUserCharacter(final User user) {


        Call<Characteristic> request = userAPI.getUserCharacter(user.getId());

        request.enqueue(new Callback<Characteristic>() {

            @Override
            public void onResponse(Call<Characteristic> call, Response<Characteristic> response) {
                Log.i("likes", response.body().getJob());

                user.setCharacteristic(response.body());
                ((UserListActivity) context).showUserDetail(user);
            }

            @Override
            public void onFailure(Call<Characteristic> call, Throwable throwable) {
                Snackbar.make(((Activity) context).findViewById(R.id.uploadListWrapper), "Something went wrong while fetching user characteristic", Snackbar.LENGTH_LONG).show();
            }

        });
    }


    /**
     *
     * Get available religions from server
     * Show religion selection dialog
     */
    public void getReligions(){
        Call<List<Religion>> request = userAPI.getReligions();

        request.enqueue(new Callback<List<Religion>>() {
            @Override
            public void onResponse(Call<List<Religion>> call, Response<List<Religion>> response) {
                ((AddCharacterActivity) context).setReligions(response.body());
            }

            @Override
            public void onFailure(Call<List<Religion>> call, Throwable throwable) {
                Snackbar.make(((Activity) context).findViewById(R.id.coordinateLayoutOnAddCharacter), "Can't fetch religions", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public void getLanguages(){
        Call<List<Language>> request = userAPI.getLanguages();

        request.enqueue(new Callback<List<Language>>() {
            @Override
            public void onResponse(Call<List<Language>> call, Response<List<Language>> response) {
                ((AddCharacterActivity) context).setLanguages(response.body());
            }

            @Override
            public void onFailure(Call<List<Language>> call, Throwable throwable) {
                Snackbar.make(((Activity) context).findViewById(R.id.coordinateLayoutOnAddCharacter), "Something went wrong", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public void getInterest(){
        Call<List<Interest>> request = userAPI.getInterests();

        request.enqueue(new Callback<List<Interest>>() {
            @Override
            public void onResponse(Call<List<Interest>> call, Response<List<Interest>> response) {
                ((AddCharacterActivity) context).setInterests(response.body());
            }

            @Override
            public void onFailure(Call<List<Interest>> call, Throwable throwable) {
                Snackbar.make(((Activity) context).findViewById(R.id.coordinateLayoutOnAddCharacter), "Something went wrong", Snackbar.LENGTH_LONG).show();
            }
        });
    }

}



