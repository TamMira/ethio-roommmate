package com.example.iyoteam.ethioroommate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.example.iyoteam.ethioroommate.actors.Room;
import com.example.iyoteam.ethioroommate.dbhandlers.RoomDBHandler;
import com.example.iyoteam.ethioroommate.R;
import com.example.iyoteam.ethioroommate.adapters.HouseRecyclerViewAdapter;

import java.util.List;

public class RoomListActivity extends AppCompatActivity {

    private HouseRecyclerViewAdapter houseRecyclerViewAdapter;
    private List<Room> rooms;
    private RecyclerView recyclerView;
    private ImageButton addNewBtn;
    private FrameLayout frameLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.house_list_layout);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        addNewBtn = findViewById(R.id.addNewBtn);
        RoomDBHandler roomDbHandler = new RoomDBHandler(this);
        roomDbHandler.getRoomList();

        addNewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RoomListActivity.this, UploadRoomActivity.class));
            }
        });
    }


    public void showAvailableRooms(List<Room> rooms){
        houseRecyclerViewAdapter = new HouseRecyclerViewAdapter(rooms, this);

        recyclerView.setAdapter(houseRecyclerViewAdapter);
    }

}
